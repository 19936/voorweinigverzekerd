import React from 'react';
import { Route, Switch } from "react-router-dom";
import './styles/global.scss';
import { Helmet } from 'react-helmet';
import HomeScreen from './screens/home/HomeScreen';
import AboutScreen from './screens/about/AboutScreen';
import InsuranceInfoScreen from './screens/insurance/InsuranceInfoScreen';
import InsurancesScreen from './screens/insurance/InsurancesScreen';

function MainRouter() {
  return (
    <div>
      <Helmet>
        <title>Voor weinig verzekerd</title>
        <meta name="description" content="Een verzekering voor weinig" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, minimum-scale=1" />
      </Helmet>
      <Switch>
        <Route exact path="/" component={HomeScreen} />
        <Route path="/about" component={AboutScreen} />
        <Route path="/insurances" component={InsurancesScreen} />
        <Route path="/insurance-info" component={InsuranceInfoScreen} />
      </Switch>
    </div>
  );
}

export default MainRouter;
