import React, { Component } from 'react';
import './AboutScreen.scss';
import Button from '../../components/button/Button'
import {Link} from "react-router-dom";

class AboutScreen extends Component {
  render() {
    return (
      <div>
        <h2>About</h2>
        <Link to="/">
          <Button text="Home"/>
        </Link>
      </div>
    );
  }
}

export default AboutScreen;
