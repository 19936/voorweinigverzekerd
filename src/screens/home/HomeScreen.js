import React, { Component } from 'react';
import axios from 'axios';
import './HomeScreen.scss';
import Button from '../../components/button/Button'
import Tips from '../../components/tips/Tips'
import InsuranceLogos from '../../components/insuranceLogos/InsuranceLogos'
import HowToSteps from '../../components/howToSteps/HowToSteps'
import WhyChoose from '../../components/whyChoose/WhyChoose'
import CarForm from '../../components/carForm/CarForm'

class HomeScreen extends Component {

  render() {
    return (
      <div className="container">
        <section className="heading">
          <div className="crossline-div">
            <div className="gradient"/>
          </div>
          <div className="top-div">
            <div className="flex-item">
              <h1>voorweinigverzekerd.nl snel, simpel, voor weinig</h1>
            </div>
          </div>
          <div className="bottom-div">
            <div className="flex-text">
              <h2>Simpel en veilig de weg op? Voorweinigverzekerd.nl helpt jou!</h2>
            </div>
            <div className="flex-button">
              <Button text="Lees meer"/>
            </div>
            <CarForm/>
          </div>
        </section>

        <InsuranceLogos/>

        {/* <HowToSteps/>

        <Tips/>

        <WhyChoose/> */}

        {/* Footer block */}
        <section className="footer"/>
      </div>
    );
  }
}

export default HomeScreen;
