import React, { Component } from 'react';
import Button from '../../components/button/Button'
import './InsurancesScreen.scss';
import './InsuranceLayout.scss';
import axios from 'axios';
import {Link} from "react-router-dom";

class Insurances extends Component {
 constructor(props) {
   super(props);
   let type = "wa";
   if (props.location.state) {
     if(props.location.state.data.insuranceType.value === "wa") {
       type = "wa";
     }
     if(props.location.state.data.insuranceType.value === "wa+") {
       type = "waplus";
     }
     if(props.location.state.data.insuranceType.value === "allrisk") {
       type = "allrisk";
     }
   }
   this.goLoad = false;
   this.timer = null;
   this.state = {
     loading: true,
     sliderValue: "200",
     type: type,
     periode: "maand",
     noRisk: false,
     noStartCost: false,
     data: [],
     openFilter: false,
   };
 }

 handleSliderChange = (event) => {
   this.setState({sliderValue: event.target.value});
   clearTimeout(this.timer);
   this.timer = setTimeout( () => {
     this._loadData();
   }, 500);
 };

 handleInputChange = (event) => {
   const target = event.target;
   const value = target.type === 'checkbox' ? target.checked : target.value;
   const name = target.name;

   this.setState({
     [name]: value
   });
   this.goLoad = true;
 };

 _loadData = () => {
   this.setState({loading: true});
   let state = this.state;
   let schadevrij = '0';
   let kilometer = '7000';
   if (this.props.location.state) {
     schadevrij = this.props.location.state.data.schadeVrij.value;
     kilometer = this.props.location.state.data.kilometer.value;
   }
   let url = 'https://cors-anywhere.herokuapp.com/19936.hosts.ma-cloud.nl/script.php?type='+state.type+'&noStartCost='+state.noStartCost+'&noRisk='+state.noRisk+'&price='+state.sliderValue+'&cleanyears='+schadevrij+'&kmyear=' + kilometer;
   axios.get(url)
     .then( (response) => {
       this.setState({loading: false, data: response.data.data})
     }).catch((err) => {
       console.log(err);
   })
 };

 componentDidUpdate() {
   if (this.goLoad === true) {
       this.goLoad = false;
     this._loadData();
   }
 }

 componentDidMount() {
   this._loadData();
 }

 Periode = (Y,M) => {
   if (this.state.periode === "maand") {
     return (
       <div className="second">
                     <span>
                       {M}
                     </span>
         <p className="price">Per maand</p>
       </div>
     );
   } else {
     return (
       <div className="second">
                     <span>
                       {Y}
                     </span>
         <p className="price">Per jaar</p>
       </div>
     );
   }

 };

 toggleFilter = () => {
   this.setState({openFilter: !this.state.openFilter});
 }

 count = () => {
   return (
     <h3>kies uit {this.state.data.length} verzekeringen</h3>
   );
 };

 BackButton = () => {
   let width = window.innerWidth;
   if (width > 768) {
   return (
     <div className="terug">
         <Link to={`/`}><Button type="cta-sm" text="Terug"/></Link>
     </div>
   );
   } else {
     return (
       <div className="terug-mobile">
       <Link to={`/`}>Terug</Link>
       </div>
     )
   }
 }

 MobileInsurances = () => {
   const loading = this.state.loading;
   let count = 0;
   let insurances = this.state.data.map((insurance) => {
     count = count + 1;
     return (
       <li className="scroll-item-outer" id={"slide-" + count}>
       <div className="scroll-item">
       <div className="single-block">
         <div className="single">
           <div className="first">
             <img src={require('../../images/unigarant.png')} alt="Unigarant logo" width="120px" height="auto"/>
           </div>
           {this.Periode(insurance.priceY, insurance.priceM)}
         </div>

         <div className="second-row">
           <div className="first">
             <span>Eenmalige kosten</span>
             <p className="price"><b>Geen</b></p>
           </div>
           <div className="first">
             <span>Eigen risico</span>
             <p className="price"><b>&euro; {insurance.risk}</b></p>
           </div>
         </div>

         <div className="third-row">
           <div className="first">
             <span>Prijs kwaliteit</span>
             <p className="price"><b>{insurance.pk}</b></p>
           </div>
           <div className="first">
             <span>Reviews</span>
             <p className="price"><b>{insurance.review}/10</b></p>
           </div>
         </div>

         <div className="fourth-row">
           <div className="first">
             <Button text="Kies deze" type="cta-sm"/>
           </div>
           <div className="second">
                     <span>

                     </span>
           </div>
         </div>

         <div className="fifth-row">
            <div className="first">
            <Link to={{
                  pathname: `/insurance-info`,
                  state: {
                    data: {
                      review: insurance.review,

                      pk: insurance.pk,

                      risk: insurance.risk,

                      price: insurance.priceM,
                    },
                  }
                }}><span>Meer informatie</span>
              </Link>
            </div>
          </div>
       </div>
       </div>
       </li>

      
     );
   });
   if (loading) {
     return <div className="loading"><img src="https://thumbs.gfycat.com/UnitedSmartBinturong-max-1mb.gif" className="gif"></img></div>;
   }
   return (
     <ul className="scroll">
       {insurances}
     </ul>
   );
 };

 Insurances = () => {
   const loading = this.state.loading;
   let insurances = this.state.data.map((insurance) => {
     return (
       <div className="single-block">
         <div className="single">
           <div className="first">
             <img src={require('../../images/unigarant.png')} alt="Unigarant logo" width="120px" height="auto"/>
           </div>
           {this.Periode(insurance.priceY, insurance.priceM)}
         </div>

         <div className="second-row">
           <div className="first">
             <span>Eenmalige kosten</span>
             <p className="price"><b>Geen</b></p>
           </div>
           <div className="first">
             <span>Eigen risico</span>
             <p className="price"><b>&euro; {insurance.risk}</b></p>
           </div>
         </div>

         <div className="third-row">
           <div className="first">
             <span>Prijs kwaliteit</span>
             <p className="price"><b>{insurance.pk}</b></p>
           </div>
           <div className="first">
             <span>Reviews</span>
             <p className="price"><b>{insurance.review}/10</b></p>
           </div>
         </div>

         <div className="fourth-row">
           <div className="first">
             <Button text="Kies deze" type="cta-sm"/>
           </div>
           <div className="second">
                     <span>

                     </span>
           </div>
         </div>

         <div className="fifth-row">
            <div className="first">
            <Link to={{
                  pathname: `/insurance-info`,
                  state: {
                    data: {
                      review: insurance.review,

                      pk: insurance.pk,

                      risk: insurance.risk,

                      price: insurance.priceM,
                    },
                  }
                }}><span>Meer informatie</span>
              </Link>
            </div>
          </div>
       </div>
     );
   });
   if (loading) {
     return <div className="loading"><img src="https://thumbs.gfycat.com/UnitedSmartBinturong-max-1mb.gif" className="gif"></img></div>;
   }
   return (
     <div className="item-block-wrapper">
       {insurances}
     </div>
   );
 };

 slideOptions = () => {
   const loading = this.state.loading;
   let count = 0;
   let options = this.state.data.map((insurance) => {
     count = count + 1;
     return (
       <a href={"#slide-" + count} className="options"></a>
     );
   });
   if (loading) {
     return;
   }
   return (
     <div className="slide-options">
       {options}
     </div>
   );
 };

 filter = () => {
   if(this.state.openFilter == true){
     return (
       <div className="mobile-filter">
         <div className="fake-single"></div>
         <div className="mobile-filter-insurance">
               <h2>Filter</h2>
               <p>Prijs</p>
               <input name="sliderValue" type="range" min="1" max="200" value={this.state.sliderValue} onChange={this.handleSliderChange} className="slider"/>
               <div className="slider-values">
                 <span className="slider-value">0</span>
                 <span className="slider-value">{this.state.sliderValue}</span>
                 </div>

              <div className="detailwrapper">
                <div className="properties">
                  <h4>Eigenschappen</h4>
                  <p><input type="checkbox" name="noRisk" checked={this.state.noRisk} onChange={this.handleInputChange}></input>Geen eigen risico</p>
                  <p><input type="checkbox" name="noStartCost" checked={this.state.noStartCost} onChange={this.handleInputChange}></input>Geen eenmalige kosten</p>
                </div>

                <div className="dekking">
                  <h4>Dekking</h4>
                  <p><input type="radio" name="type" value="wa"  checked={this.state.type == "wa"} onChange={this.handleInputChange}></input>WA</p>
                  <p><input type="radio" name="type" value="waplus"  checked={this.state.type == "waplus"} onChange={this.handleInputChange}></input>WA+</p>
                  <p><input type="radio" name="type" value="allrisk"  checked={this.state.type == "allrisk"} onChange={this.handleInputChange}></input>All Risk</p>
                </div>
              </div>

               <div className="premie">
                 <p>Premie betaling</p>
                 <select className="premie-select" name="periode" value={this.state.periode} onChange={this.handleInputChange}>
                 <option value="maand">Per maand</option>
                 <option value="jaar">Per jaar</option>
               </select>
               </div>
             </div>
         <div className="new-absolute-filter-icon" onClick={this.toggleFilter}><img src="http://www.sclance.com/pngs/white-checkmark-png/white_checkmark_png_1506465.png" className="filter-icon"></img></div>
       </div>
     );
   } else {
     return;
   }
 }

 render() {
   let width = window.innerWidth;

   if (width > 768) {
     return (
     <div>
       <section className="single-insurance">
       <h1 className="header-title">voorweinigverzekerd.nl</h1>
         <div className="terug">
       <Link to={`/`}><Button type="cta-sm" text="Terug"/></Link>
         </div>
         <div className="single-wrapper">
           <div className="fake-single"></div>
           {this.count()}
           <div className="flex-wrapper">
             <div className="filter-insurance">
               <h2>Filter</h2>
               <p>Prijs</p>
               <input name="sliderValue" type="range" min="1" max="200" value={this.state.sliderValue} onChange={this.handleSliderChange} className="slider"/>
               <div className="slider-values">
                 <span className="slider-value">0</span>
                 <span className="slider-value">{this.state.sliderValue}</span>
                 </div>

               <div className="properties">
                 <h4>Eigenschappen</h4>
                 <p><input type="checkbox" name="noRisk" checked={this.state.noRisk} onChange={this.handleInputChange}></input>Geen eigen risico</p>
                 <p><input type="checkbox" name="noStartCost" checked={this.state.noStartCost} onChange={this.handleInputChange}></input>Geen eenmalige kosten</p>
                 </div>

               <div className="dekking">
                 <h4>Dekking</h4>
                 <p><input type="radio" name="type" value="wa"  checked={this.state.type == "wa"} onChange={this.handleInputChange}></input>WA</p>
                 <p><input type="radio" name="type" value="waplus"  checked={this.state.type == "waplus"} onChange={this.handleInputChange}></input>WA+</p>
                 <p><input type="radio" name="type" value="allrisk"  checked={this.state.type == "allrisk"} onChange={this.handleInputChange}></input>All Risk</p>
               </div>

               <div className="premie">
                 <p>Premie betaling</p>
                 <select className="premie-select" name="periode" value={this.state.periode} onChange={this.handleInputChange}>
                 <option value="maand">Per maand</option>
                 <option value="jaar">Per jaar</option>
               </select>
               </div>
             </div>

             {this.Insurances()}
           </div>
         </div>
       </section>
     </div>
     );
   } else {
     return (
         <div>
         <section className="single-insurance">
           {this.BackButton()}
           <div className="single-wrapper">
             <div className="fake-single"></div>
             <h1 className="header-title -mobile">Voorweinigverzekerd.nl</h1>
             {this.count()}
             <div className="flex-wrapper">
               {/* <div className="filter-insurance">
                 <h2>Filter</h2>
                 <p>Prijs</p>
                 <input name="sliderValue" type="range" min="1" max="200" value={this.state.sliderValue} onChange={this.handleSliderChange} className="slider"/>
                 <div className="slider-values">
                   <span className="slider-value">0</span>
                   <span className="slider-value">{this.state.sliderValue}</span>
                   </div>

                 <div className="properties">
                   <h4>Eigenschappen</h4>
                   <p><input type="checkbox" name="noRisk" checked={this.state.noRisk} onChange={this.handleInputChange}></input>Geen eigen risico</p>
                   <p><input type="checkbox" name="noStartCost" checked={this.state.noStartCost} onChange={this.handleInputChange}></input>Geen eenmalige kosten</p>
                   </div>

                 <div className="dekking">
                   <h4>Dekking</h4>
                   <p><input type="radio" name="type" value="wa"  checked={this.state.type == "wa"} onChange={this.handleInputChange}></input>WA</p>
                   <p><input type="radio" name="type" value="waplus"  checked={this.state.type == "waplus"} onChange={this.handleInputChange}></input>WA+</p>
                   <p><input type="radio" name="type" value="allrisk"  checked={this.state.type == "allrisk"} onChange={this.handleInputChange}></input>All Risk</p>
                 </div>

                 <div className="premie">
                   <p>Premie betaling</p>
                   <select className="premie-select" name="periode" value={this.state.periode} onChange={this.handleInputChange}>
                   <option value="maand">Per maand</option>
                   <option value="jaar">Per jaar</option>
                 </select>
                 </div>
               </div> */}

             <div className="viewport">
               <div className="carousel">
                 {this.MobileInsurances()}
               </div>
             </div>

               {this.slideOptions()}
                 <div className="absolute-filter-icon" onClick={this.toggleFilter}><img src="http://www.sclance.com/pngs/filter-icon-png/filter_icon_png_490288.png" className="filter-icon"></img></div>
             </div>
           </div>
           {this.filter()}
         </section>
       </div>
     )
   }
 }
}

export default Insurances;
