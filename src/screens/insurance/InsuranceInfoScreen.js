import React, { Component } from 'react';
import Button from '../../components/button/Button'
import './InsuranceInfoScreen.scss';
import './InsuranceInfoLayout.scss';
import {Link} from "react-router-dom";

class InsuranceInfoScreen extends Component {
  constructor() {
    super();
      this.state = {
        open1: false,
        open2: false,
        open3: false,
        open4: false,
        open5: false,
      };
    }

  toggle = (e) => {
    console.log(e.target);
    this.setState({
      [e.target.id]: !this.state[e.target.id],
    });
  }

  render() {
    console.log(this.props.location.state.data);
    return (
      <div>
        <section className="single-insurance">
          <h1 className="header-title">voorweinigverzekerd.nl</h1>
          <div className="terug">
            <Link to={`/insurances`}><Button type="cta-sm" text="Terug"/></Link>
          </div>
          <div className="terug-mobile">
            <Link to={`/insurances`}>Terug</Link>
          </div>
          <div className="single-wrappers">
            <div className="fake-single"></div>
            <h1 className="mobile-header-title">voorweinigverzekerd.nl</h1>
            <h3>Unigarant</h3>
            <div className="single-container">
              <div className="single-block">
                <div className="single">
                  <div className="first">
                    <img src={require('../../images/unigarant.png')} alt="Unigarant logo" width="100px" height="auto"/>
                  </div>
                  <div className="second">
                    <span>{this.props.location.state.data.price}</span>
                    <p class="price">Per maand</p>
                  </div>
                </div>
                <div className="second-row">
                  <div className="first">
                    <span>Eenmalige kosten</span>
                    <p className="price"><b>Geen</b></p>
                  </div>
                  <div className="first">
                    <span>Eigen risico</span>
                    <p className="price"><b>&euro; {this.props.location.state.data.risk}</b></p>
                  </div>
                </div>
                <div className="third-row">
                  <div className="first">
                    <span>Prijs kwaliteit</span>
                    <p className="price"><b>{this.props.location.state.data.pk}</b></p>
                  </div>
                  <div className="first">
                    <span>Reviews</span>
                    <p className="price"><b>{this.props.location.state.data.review}/10</b></p>
                  </div>
                </div>
                <div className="fourth-row">
                  <div className="first">
                    <Button text="Kies deze" type="cta-sm"/>
                  </div>
                  <div className="second">
                    <span></span>
                  </div>
                </div>
                <div className="fifth-row">
                  <div className="first">
                  <Link to={`/insurances`}><span>Minder informatie</span></Link>

                  </div>
                </div>
              </div>
        <div className="lg-info-block">
         <ul>
          <li>
            <span className="properties-title">Kenmerken</span>
            <div className="down-arrow" onClick={(e) => {this.toggle(e)}}>
              <img id="open1" className={"not-rotated" + (this.state.open1 ? ' rotate-img' : '')} src={require('../../images/down-arrow.png')} alt="Down arrow"/>
            </div>
            <div className={"collapse" + (this.state.open1 ? ' in' : '')}>
              <p className="txt-bld">Eenmalige poliskosten</p>
              <span>Nee</span>
              <p className="txt-bld">Type polis</p>
              <span>Digitale polis</span>
            </div>
          </li>
          <li>
            <span className="properties-title">Pluspunten</span>
            <div className="down-arrow" onClick={(e) => {this.toggle(e)}}>
              <img id="open2" className={"not-rotated" + (this.state.open2 ? ' rotate-img' : '')} src={require('../../images/down-arrow.png')} alt="Down arrow"/>
            </div>
            <div className={"collapse" + (this.state.open2 ? ' in' : '')}>
              <p className="txt-bld">Pluspunt 1</p>
              <p className="txt-bld">Pluspunt 2</p>
              <p className="txt-bld">Pluspunt 3</p>
            </div>
          </li>
          <li>
            <span className="properties-title">Dekking</span>
            <div className="down-arrow" onClick={(e) => {this.toggle(e)}}>
              <img id="open3" className={"not-rotated" + (this.state.open3 ? ' rotate-img' : '')} src={require('../../images/down-arrow.png')} alt="Down arrow"/>
            </div>
            <div className={"collapse" + (this.state.open3 ? ' in' : '')}>
              <p className="txt-bld">Beperkt Casco WA +</p>
              <ul>
                <li>
                  <img src={require('../../images/blue-check.png')} alt="Checkmark" />
                  Schade aan anderen
                </li>
                <li>
                  <img src={require('../../images/blue-check.png')} alt="Checkmark" />
                  Diefstal en inbraak
                </li>
                <li>
                  <img src={require('../../images/blue-check.png')} alt="Checkmark" />
                  Brand, storm, natuur
                </li>
                <li>
                  <img src={require('../../images/blue-check.png')} alt="Checkmark" />
                  Ruitschade
                </li>
              </ul>
            </div>
          </li>
          <li>
            <span className="properties-title">Prijskwaliteit</span>
            <div className="down-arrow" onClick={(e) => {this.toggle(e)}}>
              <img id="open4" className={"not-rotated" + (this.state.open4 ? ' rotate-img' : '')} src={require('../../images/down-arrow.png')} alt="Down arrow"/>
            </div>
            <div className={"collapse" + (this.state.open4 ? ' in' : '')}>
              <p className="txt-bld">De prijs kwaliteit verhouding is Uitstekend</p>
              <p className="txt-bld">Deze verzekering staat op positie 1 van de 5</p>
            </div>
          </li>
          <li>
            <span className="properties-title">Reviews</span>
            <div className="down-arrow" onClick={(e) => {this.toggle(e)}}>
              <img id="open5" className={"not-rotated" + (this.state.open5 ? ' rotate-img' : '')} src={require('../../images/down-arrow.png')} alt="Down arrow"/>
            </div>
            <div className={"collapse" + (this.state.open5 ? ' in' : '')}>
              <p className="txt-bld">De reviews scoren een 8,6/10</p>
            </div>
          </li>
         </ul>
        </div>
        </div>
      </div>
    </section>
    </div>
    );
  }
}

export default InsuranceInfoScreen;
