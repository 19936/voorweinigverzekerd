import React, { Component } from 'react';
import './Tips.scss';

class Tips extends Component {

  constructor (props){
    super(props);
    this.state = {
      arrayIndex: 0,
    };
  }

  incrementIndex = () => {
    let val = this.state.arrayIndex + 1;
    if(val > this.tips.length - 1) {
      val = 0;
    }
    this.setState({arrayIndex: val});
  }

  decrementIndex = () => {
    let val = this.state.arrayIndex - 1;
    if(val < 0) {
      val = this.tips.length - 1;
    }
    this.setState({arrayIndex: val});
  }

  tips = [{title:'Schadevrije jaren?', paragraph:'Lorem ipsum'}, {title:'All risk verzekeren?', paragraph:'Dolor sit'}, {title:'Inzittende ook gedekt?', paragraph:'Amet consectetur'}];



  render() {
    return (
      <section className="tips">
        <h2>Handige tips voor het afsluiten van je autoverzekering.</h2>
        <div className="tips-bg">
          <div className="tips-txt-box">
            <h4>{this.tips[this.state.arrayIndex].title}</h4>
            <p>{this.tips[this.state.arrayIndex].paragraph}</p>
          </div>
          <div className="controls">
            <input className="ctrl-btn" type="button" onClick={this.incrementIndex} value=">"/>
            <input className="ctrl-btn" type="button" onClick={this.decrementIndex} value="<"/>
          </div>
        </div>
      </section>
    );
  }
}

export default Tips;
