import React, { Component } from 'react';
import axios from 'axios';
import './CarForm.scss';
import Button from '../../components/button/Button'
import { withRouter } from 'react-router-dom';
import {Link} from "react-router-dom";

class CarForm extends Component {

  state = {
    formIndex: 0,
    insuranceType:{value:'wa'},
    zipCode:{value:'', error: false},
    kenteken:{value:'', valid:false, error: false},
    carName:{value:''},
    houseNumber:{value:'', error: false},
    additional:{value:''},
    stayUnknown: {value:false},
    kilometer: {value:'0', error:false},
    geboorteDatum: {value:''},
    schadeVrij: {value:'', error:false},
  };

  handleChange = (e) => {
    let state = {...this.state[e.target.name]};
    state.value = e.target.value;
    this.setState({[e.target.name]: state});
    if (e.target.name === "kenteken") {
      this.handleKenteken(e);
    }
  };

  handleCheckbox = (e) => {
    let state = {...this.state[e.target.name]};
    state.value = e.target.checked;
    this.setState({[e.target.name]: state});
  };

  handleOptionChange = (e) => {
    this.setState({insuranceType: {value: e.target.value}});
  };

  decrementFormIndex = () => {
    let newFormIndex = this.state.formIndex - 1;
    this.setState({formIndex: newFormIndex});
  }


  handleKenteken = (e) => {
    let carName = {...this.state.carName};
    let stayUnknown = {...this.state.stayUnknown};

    let val = e.target.value.toUpperCase();
    val = val.replace(/-/g, '');
    carName.valid = false;
    carName.value = '';

      axios.get('https://opendata.rdw.nl/resource/m9d7-ebf2.json?kenteken=' + val)
        .then( (response) => {
          if (response.data.length > 0) {
            let merk = response.data[0].merk;
            let hname = response.data[0].handelsbenaming;

            if (hname.indexOf(merk) > -1) {
              carName.value = hname
            } else {
              carName.value = merk + " " + hname
            }

            carName.valid = true;
            stayUnknown.value = false;
            this.setState({carName, stayUnknown})
          } else {
            this.setState({carName})
          }
        })
        .catch( (error) => {
          this.setState({carName})
        });
  };

  validateZipcode(val) {
    let regex = /^[1-9][0-9]{3}[\s]?[A-Za-z]{2}$/i;
    return regex.test(val);
  }

  validateStepOne = () => {
    if (this.state.stayUnknown.value === false) {
      let kenteken = {...this.state.kenteken};
      let zipCode = {...this.state.zipCode};
      let houseNumber = {...this.state.houseNumber};

      //check if kenteken is valid
      kenteken.error = !this.state.carName.valid;

      //check if housenumber is valid
      if (this.state.houseNumber.value > 0) {
        houseNumber.error = isNaN(houseNumber.value);
      } else {
        houseNumber.error = true;
      }

      //check if zipcode is valid
      zipCode.error = !this.validateZipcode(this.state.zipCode.value);

      if (zipCode.error || kenteken.error || houseNumber.error) {
        this.setState({kenteken, zipCode, houseNumber});
      } else {
        this.setState({kenteken, zipCode, houseNumber});
        this.setState({formIndex: 1})
      }
    } else {
      this.props.history.push('/insurances')
    }
  };

  validateStepTwo= () => {
    let schadeVrij = {...this.state.schadeVrij};
    let geboorteDatum = {...this.state.geboorteDatum};


    //check if schadeVrij is valid
    if (this.state.schadeVrij.value > 0) {
      schadeVrij.error = isNaN(schadeVrij.value);
    } else {
      schadeVrij.error = true;
    }

    //check if geboorteDatum is valid
    let date = geboorteDatum.value.replace(/-/g, '');
    if (date > 0) {
      geboorteDatum.error = isNaN(date);
    } else {
      geboorteDatum.error = true;
    }

    if (geboorteDatum.error || schadeVrij.error) {
      this.setState({geboorteDatum, schadeVrij});
    } else {
      this.setState({geboorteDatum, schadeVrij});
      this.setState({formIndex: 2})
    }
  };


  renderCar() {
    let carName = this.state.carName.value;
    if (carName.length > 19) {
      carName = carName.substr(0,17) + "..";
    }
      return (
      <div className="mt m-l">
        <span className="kenteken">{carName}</span>
      </div>
    )
  }

  UnknownCheckbox() {
    return (
      <div className="mt m-l">
        <input type="checkbox" name="stayUnknown" id="unknown" hidden onChange={(e) => {this.handleCheckbox(e)}}/>
        <label htmlFor="unknown" className={`custom-checkbox checkbox-${this.state.stayUnknown.value}`}><img src={require('../../images/blue-check.png')} alt="checkbox"/></label>
        <label htmlFor="unknown" className="kenteken">Onbekend</label>
      </div>
    )
  }

  FormStepOne() {
    return (
      <div className="form">
        <div className="form-wrapper">
          <h1>Vul je gegevens <br/>
            in en vergelijk direct </h1>
          <div className="kenteken-wrap">
            <div className="flex-wrap">
              <span className="kenteken">Kenteken</span>
              <br/>
              <input disabled={this.state.stayUnknown.value} type="text" className={`${this.state.kenteken.error ? "error " : ""}${this.state.stayUnknown.value ? "disabled" : ""}`} name="kenteken" placeholder="12-AB-34" onChange={(e) => {this.handleChange(e)}}/>
            </div>
            <div className="flex-wrap">
              <br/>
              {this.state.carName.valid ? this.renderCar() : this.UnknownCheckbox()}
            </div>
          </div>

          <div className="kenteken-wrap">
            <div className="flex-wrap">
              <span className="kenteken">Postcode <span className="required"> *</span></span>
              <br/>
              <input type="text" className={`${this.state.zipCode.error ? "error " : ""}`} placeholder="1234 AA"  value={this.state.zipCode.value} name="zipCode" onChange={(e) => {this.handleChange(e)}}/>
            </div>
            <div className="flex-wrap">
              <span className="kenteken m-l">Huisnummer <span className="required"> *</span></span>
              <br/>
              <input type="text" className={`${this.state.houseNumber.error ? "adress m-l error " : "adress m-l "}`} value={this.state.houseNumber.value} placeholder="19" name="houseNumber" onChange={(e) => {this.handleChange(e)}}/>
              <input type="text" className={`adress`} placeholder="Toev." name="additional" onChange={(e) => {this.handleChange(e)}}/>
            </div>
            <div className="flex-center">
              <Button type="cta" text="Verder" onClick={this.validateStepOne}/>
            </div>
          </div>
        </div>
      </div>
    );
  }

  FormStepTwo() {
    return (
      <div className="form">
        <div className="form-wrapper">
          <h1>Vul je gegevens <br/>
            in en vergelijk direct </h1>
          <div className="kenteken-wrap">
            <div className="flex-wrap">
              <span className="kenteken">Schade vrije jaren <span className="required"> *</span></span>
              <br/>
              <input type="text" className={this.state.schadeVrij.error ? "error" : ""} name="schadeVrij" value={this.state.schadeVrij.value} placeholder="0" onChange={(e) => {this.handleChange(e)}}/>
            </div>
            <div className="flex-wrap">
              <span className="kenteken m-l">Kilometer per jaar<span className="required"> *</span></span>
              <br/>
              <select className="m-l" name="kilometer" value={this.state.kilometer.value} onChange={(e) => {this.handleChange(e)}}>
                <option value="0">0 tot 7000</option>
                <option value="7000">7000 tot 14000</option>
                <option value="12000">12000 to 20000</option>
                <option value="20000">20000 to 30000</option>
                <option value="30000">30000 to 40000</option>
                <option value="40000">40000+</option>
              </select>
            </div>
          </div>

          <div className="kenteken-wrap">
            <div className="flex-wrap">
              <span className="kenteken">Geboortedatum <span className="required"> *</span></span>
              <br/>
              <input type="text" className={this.state.geboorteDatum.error ? "error" : ""} value={this.state.geboorteDatum.value} name="geboorteDatum" placeholder="dd-mm-jjjj" onChange={(e) => {this.handleChange(e)}}/>
            </div>
            <div className="flex-center">
              <div className="flex-half first">
                <Button text="Terug" onClick={this.decrementFormIndex}/>
              </div>
              <div className="flex-half second">
                <Button type="cta" text="Verder" onClick={this.validateStepTwo}/>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  FormStepThree() {
    return (
      <div className="form">
        <div className="form-wrapper">
          <h1>Hoe wil je <br/>
            gedekt zijn? </h1>
          <div className="insurance-types three">
            <div className="type-wrapper">
              <label className="type-label">
                <input type="radio" value="wa" name="wa" hidden checked={this.state.insuranceType.value === 'wa'} onChange={this.handleOptionChange} />
                <div className="info-button">
                  ?
                  <div className="help-wrapper">
                    <p>De verplichte dekking. Schade die je met je auto bij anderen veroorzaakt binnen de voorwaarden wordt vergoed.</p>
                  </div>
                </div>
                <h4>WA</h4>
                <div className="circle-wrapper">
                  <div className={this.state.insuranceType.value === 'wa' ? "circle" : ""}/>
                </div>
              </label>
            </div>
            <div className="type-wrapper">
              <label className="type-label">
                <input type="radio" value="wa+" name="wa+" hidden checked={this.state.insuranceType.value === 'wa+'} onChange={this.handleOptionChange} />
                <div className="info-button">
                  ?
                  <div className="help-wrapper">
                    <p>De WA dekking plus zaken als diefstal, inbraak, ruit-, brand- of stormschade worden vergoed aan eigen voertuig. Eigen schade niet.</p>
                  </div>
                </div>
                <h4>WA+</h4>
                <div className="circle-wrapper">
                  <div className={this.state.insuranceType.value === 'wa+' ? "circle" : ""}/>
                </div>
              </label>
            </div>
            <div className="type-wrapper">
              <label className="type-label">
                <input type="radio" value="allrisk" name="allrisk" hidden checked={this.state.insuranceType.value === 'allrisk'} onChange={this.handleOptionChange} />
                <div className="info-button">
                  ?
                  <div className="help-wrapper">
                    <p>Schade door bijvoorbeeld een ongeluk of anderen gebeurtenissen die je zelf hebt veroorzaakt worden ook vergoed.</p>
                  </div>
                </div>
                <h4>All risk</h4>
                <div className="circle-wrapper">
                  <div className={this.state.insuranceType.value === 'allrisk' ? "circle" : ""}/>
                </div>
              </label>
            </div>
          </div>
          <div className="kenteken-wrap">
            <div className="flex-center">
              <div className="flex-half first">
                <Button text="Terug" onClick={this.decrementFormIndex}/>
              </div>
              <div className="flex-half second">
                <Link to={{
                  pathname: `/insurances`,
                  state: {
                    data: this.state,
                  }
                }}><Button type="cta" text="Vergelijk" onClick={this.validateStepThree}/></Link>

              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  renderForm() {
    switch (this.state.formIndex) {
      case 0:
        return (
          this.FormStepOne()
        );
      case 1:
        return (
          this.FormStepTwo()
        );
      case 2:
        return (
          this.FormStepThree()
        );
    }
  }


  render() {
    return (
      <div>
        {this.renderForm()}
      </div>
    )
  }
}

export default withRouter(CarForm);
