import React, { Component } from 'react';
import './HowToSteps.scss';

class HowToSteps extends Component {
  render() {
    return (
    <section className="steps">
      <h2>Hoe sluit je een verzekering af?</h2>
      <div className="steps-wrapper">
        <div className="steps-txt">
          <h3>Sluit je verzekering af in 3 stappen.</h3>
          <p>Dit is een tekstblok voor kleine informatie.</p>
        </div>
        <div className="steps-media">
          <div className="steps-video-bg">
            <div className="steps-video-player">
            <iframe className="iframe" 
            src="https://www.youtube.com/embed/BDq0_kJAP6U" 
            frameborder="0" allow="accelerometer; autoplay;" controls="0" allowfullscreen>
            </iframe>
              <div className="play-btn">
                <img src={require('../../images/playbutton.png')} alt="Playbutton"/>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    );
  }
}

export default HowToSteps;
