import React, { Component } from 'react';
import './WhyChoose.scss';

class WhyChoose extends Component {
  render() {
    return (
      <section className="choose">
        <h2>Waarom kiezen voor voorweinigverzekerd.nl?</h2>
        <div className="choose-wrapper">
          <div className="item-1">
            <div className="item-box">
              <div className="top-small-div">
                <div className="small-box"/>
              </div>
              <div className="bottom-small-div">
                <h2>Snel</h2>
                <h3>Dit is een tekst waarom de mensen moeten gaan kiezen voor voorweinigverzekerd.nl</h3>
              </div>
            </div>
          </div>
          <div className="item-1">
            <div className="item-box -second">
              <div className="top-small-div">
                <div className="small-box"/>
              </div>
              <div className="bottom-small-div">
                <h2>Simpel</h2>
                <h3>Dit is een tekst waarom de mensen moeten gaan kiezen voor voorweinigverzekerd.nl</h3>
              </div>
            </div>
          </div>
          <div className="item-1">
            <div className="item-box -third">
              <div className="top-small-div">
                <div className="small-box"/>
              </div>
              <div className="bottom-small-div">
                <h2>Voor weinig</h2>
                <h3>Dit is een tekst waarom de mensen moeten gaan kiezen voor voorweinigverzekerd.nl</h3>
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default WhyChoose;
