import React, { Component } from 'react';
import './InsuranceLogos.scss';

class InsuranceLogos extends Component {
  render() {
    return (
      <section className="insurances">
        <div className="isc-wrapper">
          <div className="box one">
            <img src={require('../../images/fbto.png')} alt="FBTO logo"/>
          </div>
          <div className="box two">
            <img src={require('../../images/allianz.png')} alt="Allianz logo"/>
          </div>
          <div className="box three">
            <img src={require('../../images/unive.png')} alt="Unive logo"/>
          </div>
          <div className="box four">
            <img src={require('../../images/ohra.png')} alt="Ohra logo"/>
          </div>
          <div className="box five">
            <img src={require('../../images/unigarant.png')} alt="Unigarant logo"/>
          </div>
          <div className="box six">
            <img src={require('../../images/reaal.png')} alt="Reaal logo"/>
          </div>
          <div className="box seven">
            <img src={require('../../images/allsecur.png')} alt="Allsecur logo"/>
          </div>
          <div className="box eight">
            <img src={require('../../images/centraalbeheer.png')} alt="CB logo"/>
          </div>
        </div>
      </section>
    );
  }
}

export default InsuranceLogos;
