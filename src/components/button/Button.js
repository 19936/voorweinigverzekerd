import React, { Component } from 'react';
import './Button.scss';

class Button extends Component {

  render() {
    return (
      <div className={this.props.type ? this.props.type : "btn"} onClick={this.props.onClick}>
        {this.props.text}
      </div>
    );
  }
}

export default Button;
